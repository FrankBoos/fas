# FAS3.0 凌一破解Shell版
FAS3.0 stream control script 一键脚本

## 系统要求
1. 内存/硬盘	内存最低512M 硬盘至少200M
2. 操作系统	centos7.0或更高
3. 网络	宽带推荐至少10Mbps 取决于您负荷的人数 必须具有固定公网IP


```bash
 wget https://gitee.com/FrankBoos/fas/raw/master/fas.sh;bash fas.sh
```